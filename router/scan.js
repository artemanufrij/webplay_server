var express = require("express");
var file_scanner = require("../services/files_scanner");
var cover_excluder = require ("../services/cover_excluder");
var notifier = require("../services/notifier");

var server = require("../server");
var status = server.status;
var config = server.config;


var router = new express.Router();

router.route("/music").post((req, res) => {
  if (!status.scanning_music) {
    status.scanning_music = true;
    console.log("music scann started…");
    res.send("music scann started…");
    file_scanner.scann_local_music_files(config.music_folder);
  } else {
    res.send("please wait. server is scanning for music files…");
  }
});

router.route("/video").post((req, res) => {
  if (!status.scanning_video) {
    status.scanning_video = true;
    console.log("video scann started…");
    res.send("video scann started…");
    file_scanner.scann_local_video_files(config.video_folder);
  } else {
    res.send("please wait. server is scanning for video files…");
  }
});

router.route("/cover/albums").post((req, res) => {
  cover_excluder.checkMissingCovers();
});

router.route("/cover/artists").get((req, res) => {
  cover_excluder.checkMissingArtistCovers();
  res.end ();
});

module.exports = router;