var express = require("express");
var router = new express.Router();

var db_manager = require("../services/db_manager");

var server = require("../server");
var config = server.config;

router
  .route("/setup")
  .get((req, res) => {
    db_manager.users(users => {
      if (users && users.length > 0) {
        res.redirect("/user/login");
      } else {
        console.log("setup");
        res.sendFile(config.base_path + "/client/setup.html");
      }
    });
  })
  .post((req, res) => {
    console.log("add admin user");
    let newUser = {
       name: req.body.username,
       password: req.body.password,
       roles: ["admin"]
    }
    db_manager.addUser(newUser, result => {
      res.redirect("/user/login");
    });
  });

module.exports = router;
