var express = require("express");
var router = new express.Router();

var server = require("../server");
var status = server.status;

var db_manager = require("../services/db_manager");
var tag_excluder = require("../services/tag_excluder");

router.route("").get((req, res) => {
  status.scann_buffer = tag_excluder.get_buffer_size();
  db_manager.artist_count((q, c) => {
    status.artists = c;
    db_manager.album_count((q, c) => {
      status.albums = c;
      db_manager.track_count((q, c) => {
        status.tracks = c;
        db_manager.box_count((q, c) => {
          status.boxes = c;
          db_manager.video_count((q, c) => {
            status.videos = c;
            status.parsing_music_data = tag_excluder.parsing_music_data();
            status.parsing_video_data = tag_excluder.parsing_video_data ();
            status.parsing_cover = tag_excluder.parsing_cover();
            res.json(status);
          });
        });
      });
    });
  });
});

module.exports = router;
