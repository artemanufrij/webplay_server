var express = require("express");
var fs = require("fs");
var db_manager = require("../services/db_manager");

var server = require("../server");
var config = server.config;

var router = new express.Router();

router
  .route("")
  .get((req, res) => {
    db_manager.radios(result => {
      res.json(result);
    });
  })
  .post((req, res) => {
    console.log("add radio");
    if (req.body.name == "" || !req.body.url.startsWith("http")) {
      res.end();
    }
    let newRadio = {
      name: req.body.name,
      url: req.body.url
    };
    db_manager.addRadio(newRadio, () => {
      res.end();
    });
  })
  .delete((req, res) => {
    console.log("delete radio");
    db_manager.deleteRadio(req.body.id, () => {
      db_manager.radios(result => {
        res.json(result);
      });
    });
  });

router.route("/:id").get((req, res) => {
  db_manager.box(req.params.id, false, result => {
    res.json(result);
  });
});

router.route("/:id/cover/:size").get((req, res) => {
  res.header({
    "Content-Type": "image/jpg"
  });
  let cover =
    config.cache_folder +
    "/radio_covers/" +
    req.params.size +
    "/" +
    req.params.id;
  if (fs.existsSync(cover)) {
    res.sendFile(cover);
  } else {
    res.end();
  }
});

module.exports = router;
