var express = require("express");
var fs = require("fs");
var db_manager = require("../services/db_manager");

var server = require("../server");
var config = server.config;

var router = new express.Router();

router.route("").get((req, res) => {
  db_manager.albums(result => {
    res.json(result);
  });
});

router.route("/:id").get((req, res) => {
  db_manager.album(req.params.id, false, result => {
    res.json(result);
  });
});

router.route("/:id/cover/:size").get((req, res) => {
  let cover =
    config.cache_folder +
    "/album_covers/" +
    req.params.size +
    "/" +
    req.params.id;
  if (fs.existsSync(cover)) {
    var bitmap = fs.readFileSync(cover);
    res.send(
      "data:image/jpeg;base64, " + Buffer.from(bitmap).toString("base64")
    );
  }
  res.end();
});

module.exports = router;
