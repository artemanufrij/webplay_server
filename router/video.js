var express = require("express");
var fs = require("fs");
var ffmpeg = require("fluent-ffmpeg");
var db_manager = require("../services/db_manager");

var server = require("../server");
var config = server.config;

var router = new express.Router();

var video_cache_dir = config.cache_folder + "/video_cache/";

if (!fs.existsSync(video_cache_dir)) {
  fs.mkdirSync(video_cache_dir);
}

server.lists.video_quality.forEach(rate => {
  let dir = video_cache_dir + rate;
  if (!fs.existsSync(dir)) {
    fs.mkdir(dir, () => {});
  }
});

router.route("/:id/lang").get((req, res) => {
  db_manager.video(req.params.id, result => {
    process.stdout.write("video lang: " + result.title);
    ffmpeg(config.video_folder + result.path).ffprobe((err, data) => {
      let audio_streams = data.streams.filter(f => f.codec_type == "audio");
      let return_value = audio_streams.map(m => {
        return {
          index: m.index,
          lang: (m.tags && m.tags.language
            ? m.tags.language
            : m.index
            ? m.index
            : ""
          ).toString(),
          title: (m.tags && m.tags.title
            ? m.tags.title
            : m.index
            ? m.index
            : ""
          ).toString()
        };
      });
      res.json(return_value);
    });
  });
});

router.route("/:id/stream").get((req, res) => {
  db_manager.video(req.params.id, result => {
    process.stdout.write("video stream: " + result.title);
    if (!fs.existsSync(config.video_folder + result.path)) {
      return res.end();
    }
    stream(req, res, config.video_folder + result.path, result.mime);
  });
});

router.route("/:id/stream/:rate/:audioIndex").get((req, res) => {
  if (!server.lists.video_quality.includes(req.params.rate)) {
    req.end();
  }

  let video = getFileName(req);
  if (fs.existsSync(video)) {
    stream(req, res, video, "video/mpeg");
  } else {
    convert(req, res, video, () => {
      stream(req, res, video, "video/mp4");
    });
  }
});

router.route("/:id/convert/:rate/:audioIndex").get((req, res) => {
  if (!server.lists.video_quality.includes(req.params.rate)) {
    req.end();
  }
  let video = getFileName(req);
  if (!fs.existsSync(video)) {
    convert(req, res, video);
  }
  res.end();
});

/* router.route("/:id/thumbnail").get((req, res) => {
  let cover =
    config.cache_folder + "/video_covers" + "/" + req.params.id + ".png";
  if (fs.existsSync(cover)) {
    let bitmap = fs.readFileSync(cover);
    let base64 =
      "data:image/png;base64, " + new Buffer(bitmap).toString("base64");
    res.send(base64);
  }
  res.end();
}); */

function convert(req, res, video, callback = null) {
  db_manager.video(req.params.id, result => {
    process.stdout.write("video stream: " + result.title);
    if (!fs.existsSync(config.video_folder + result.path)) {
      return res.end();
    }

    ffmpeg(config.video_folder + result.path)
      .audioBitrate(128)
      .audioChannels(2)
      .videoCodec("libvpx")
      .outputOptions([
        "-deadline " + req.params.rate,
        "-cpu-used 2",
        "-map 0:v:0",
        "-map 0:a:" + req.params.audioIndex
      ])
      .output(video)
      .on("start", commandLine => {
        process.stdout.write("Spawned Ffmpeg with command: " + commandLine);
        if (callback) {
          let interval = setInterval(() => {
            if (fs.existsSync(video) && fs.statSync(video).size > 3000000) {
              clearInterval(interval);
              callback();
            }
          }, 4000);
        }
      })
      .on("error", e => {
        process.stdout.write(e);
      })
      .run();
  });
}

function stream(req, res, video, mime) {
  process.stdout.write("video stream: " + video);
  let stat = fs.statSync(video);

  if (req.headers.range) {
    var range = req.headers.range;
    var parts = range.replace(/bytes=/, "").split("-");
    var partialstart = parts[0];
    var partialend = parts[1];

    var start = parseInt(partialstart, 10);
    var end = partialend ? parseInt(partialend, 10) : stat.size - 1;
    var chunksize = end - start + 1;
    var readStream = fs.createReadStream(video, { start: start, end: end });
    res.writeHead(206, {
      "Content-Range": "bytes " + start + "-" + end + "/" + stat.size,
      "Accept-Ranges": "bytes",
      "Content-Length": chunksize,
      "Content-Type": mime
    });
    readStream.pipe(res);
  } else {
    res.writeHead(200, {
      "Content-Length": stat.size,
      "Content-Type": mime
    });
    fs.createReadStream(video).pipe(res);
  }
}

function getFileName(req) {
  return (
    video_cache_dir +
    req.params.rate +
    "/" +
    req.params.id +
    "-" +
    req.params.audioIndex +
    ".webm"
  );
}

module.exports = router;
