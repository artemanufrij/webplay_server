var express = require("express");
var fs = require("fs");
var db_manager = require("../services/db_manager");
var node_id3 = require("node-id3");
var Lame = require("node-lame").Lame;
var Fdkaac = require("node-fdkaac").Fdkaac;

var server = require("../server");
var config = server.config;

var router = new express.Router();

var audio_cache_dir = config.cache_folder + "/audio_cache/";

if (!fs.existsSync(audio_cache_dir)) {
  fs.mkdirSync(audio_cache_dir);
}

server.lists.audio_quality.forEach(bmp => {
  let dir = audio_cache_dir + bmp;
  if (!fs.existsSync(dir)) {
    fs.mkdir(dir, () => {});
  }
});

router.route("/:id/stream").get((req, res) => {
  db_manager.track(req.params.id, result => {
    process.stdout.write("audio stream: " + result.title);
    if (!fs.existsSync(config.music_folder + result.path)) {
      return res.end();
    }
    stream(req, res, config.music_folder + result.path, result.mime);
  });
});

router.route("/:id/stream/:rate").get((req, res) => {
  if (!server.lists.audio_quality.includes(req.params.rate)) {
    req.end();
  }

  let music = audio_cache_dir + req.params.rate + "/" + req.params.id + ".mp3";

  if (fs.existsSync(music)) {
    stream(req, res, music, "audio/mpeg");
  } else {
    db_manager.track(req.params.id, result => {
      if (!fs.existsSync(config.music_folder + result.path)) {
        return res.end();
      }

      if (
        result.mime == "audio/mpeg" &&
        result.bitrate / 1000 <= req.params.rate
      ) {
        process.stdout.write("stream non converted file");
        stream(req, res, config.music_folder + result.path, result.mime);
        return;
      }
      process.stdout.write("audio convert: " + result.title + " (" + result.mime + ")");

      if (result.mime.includes("mp4")) {
        process.stdout.write("audio decode: " + result.title + " (" + result.mime + ")");
        const decoder = new Fdkaac({
          output: "buffer"
        }).setFile(config.music_folder + result.path);
        decoder
          .decode()
          .then(() => {
            const buffer = decoder.getBuffer();
            const lame_encoder = new Lame({
              output: music,
              bitrate: req.params.rate
            }).setBuffer(buffer);

            lame_encoder
              .encode()
              .then(() => {
                node_id3.removeTags(music);
                stream(req, res, music, "audio/mpeg");
              })
              .catch(err => {
                process.stdout.write(err);
              });
          })
          .catch(err => {
            process.stdout.write(err);
          });
      } else {
        const lame_encoder = new Lame({
          output: music,
          bitrate: req.params.rate
        }).setFile(config.music_folder + result.path);
        lame_encoder
          .encode()
          .then(() => {
            node_id3.removeTags(music);
            stream(req, res, music, "audio/mpeg");
          })
          .catch(err => {
            process.stdout.write(err);
            process.stdout.write("stream original");
            stream(req, res, config.music_folder + result.path, result.mime);
          });
      }
    });
  }
});

function stream(req, res, music, mime) {
  process.stdout.write("audio stream: " + music);
  let stat = fs.statSync(music);

  if (req.headers.range) {
    var range = req.headers.range;
    var parts = range.replace(/bytes=/, "").split("-");
    var partialstart = parts[0];
    var partialend = parts[1];

    var start = parseInt(partialstart, 10);
    var end = partialend ? parseInt(partialend, 10) : stat.size - 1;
    var chunksize = end - start + 1;
    var readStream = fs.createReadStream(music, { start: start, end: end });
    res.writeHead(206, {
      "Content-Range": "bytes " + start + "-" + end + "/" + stat.size,
      "Accept-Ranges": "bytes",
      "Content-Length": chunksize,
      "Content-Type": mime
    });
    readStream.pipe(res);
  } else {
    res.writeHead(200, {
      "Content-Length": stat.size,
      "Content-Type": mime
    });
    fs.createReadStream(music).pipe(res);
  }
}

module.exports = router;
