var express = require("express");
var router = new express.Router();
var bcrypt = require("bcryptjs");
var db_manager = require("../services/db_manager");
var jwt = require("jsonwebtoken");

var server = require("../server");
var passport = server.passport;
var secret = server.secret;

router
  .route("/me")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    delete req.user.password;
    let user = req.user;

    db_manager.historyList(user._id, history => {
      user.history = history;
      res
        .status(200)
        .json(req.user)
        .end();
    });
  });

router
  .route("/login")
  .get(() => {
    db_manager.users(users => {
      if (users && users.length > 0) {
        //res.sendFile(config.base_path + "/client/login.html");
      } else {
        //res.redirect("/system/setup");
      }
    });
  })
  .post((req, res) => {
    db_manager.userByName(req.body.username, user => {
      if (!user) {
        process.stdout.write("no user in DB\n");
        res.status(401).send({
          success: false,
          msg: "Authentication failed. Wrong user."
        });
      }

      bcrypt.compare(req.body.password, user.password, (err, isMatch) => {
        if (err) throw err;
        if (isMatch) {
          process.stdout.write("pass ok\n");
          db_manager.updateUserAccess(req.body.username);
          var token = jwt.sign(user, secret);
          delete user.password;
          user.token = "JWT " + token;
          res.status(200).json(user);
        } else {
          process.stdout.write("pass fail\n");
          res.status(401).send({
            success: false,
            msg: "Authentication failed. Wrong password."
          });
        }
      });
    });
  });

module.exports = router;
