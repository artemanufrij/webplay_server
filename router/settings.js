var express = require("express");
var router = new express.Router();

var server = require("../server");

router.route("/audio/quality/list").get((req, res) => {
  res.json(server.lists.audio_quality);
});

router.route("/video/lang/list").get((req, res) => {
  res.json(server.lists.lang);
});

router.route("/video/quality/list").get((req, res) => {
  res.json(server.lists.video_quality);
});

router.route("/user/roles").get((req, res) => {
  res.json(server.lists.user_role);
});

module.exports = router;
