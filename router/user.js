var express = require("express");
var db_manager = require("../services/db_manager");
var bcrypt = require("bcryptjs");
var router = new express.Router();

var server = require("../server");
var passport = server.passport;

router
  .route("")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    db_manager.users(result => {
      res.json(result).end();
    });
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("add user\n");
    if (
      req.user.roles.includes("admin") ||
      req.user.roles.includes("moderator")
    ) {
      let newUser = {
        name: req.body.name,
        password: req.body.password
      };
      db_manager.addUser(newUser, () => {
        res.end();
      });
    } else {
      res.status(401).end();
    }
  })
  .delete(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("delete user\n");
    if (req.user.roles.includes("admin") || req.user._id == req.query.id) {
      db_manager.deleteUser(req.query.id, () => {
        db_manager.users(result => {
          res.json(result).end();
        });
      });
    } else {
      res.status(401).end();
    }
  })
  .put(passport.authenticate("jwt", { session: false }), (req, res) => {
    process.stdout.write("update user\n");
    if (req.user.roles.includes("admin")) {
      if (req.body.newPassword) {
        db_manager.updateUserPassword(req.body.name, req.body.newPassword);
      } else {
        db_manager.updateUserRole(req.body, () => {
          res.status(202).end();
        });
      }
      res.end();
    } else {
      res.status(401).end();
    }
  });

router
  .route("/:name/exists")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (
      req.user.roles.includes("admin") ||
      req.user.roles.includes("moderator")
    ) {
      db_manager.userByName(req.params.name, user => {
        res.json({ exists: user != null }).end();
      });
    } else {
      res.status(401).end();
    }
  });

router
  .route("/update")
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    if (!req.user) {
      return res.status(401).end();
    }
    if (req.body.restorePath != null) {
      db_manager.updateUserConfig(req.user, req.body, () => {
        if (!req.body.restorePath) {
          req.user.path = "";
          db_manager.updateUserSettings(req.user);
        }
        process.stdout.write("config changed\n");
      });
    }

    if (req.body.oldPassword && req.user.password) {
      bcrypt.compare(req.body.oldPassword, req.user.password, function(
        err,
        isMatch
      ) {
        if (err) throw err;
        if (isMatch) {
          db_manager.updateUserPassword(req.user, req.body.newPassword);
          res.status(202);
        } else {
          res.status(422);
        }
        res.end();
      });
    } else {
      res.end();
    }
  });

router
  .route("/history")
  .get(passport.authenticate("jwt", { session: false }), (req, res) => {
    db_manager.historyList(req.user._id, result => {
      res.json(result);
    });
  })
  .post(passport.authenticate("jwt", { session: false }), (req, res) => {
    let item = { id: req.body.id, type: req.body.type };
    db_manager.updateHistory(req.user._id, item, () => {
      db_manager.historyList(req.user._id, result => {
        res.json(result);
      });
    });
  });

router.route("/settings").put((req, res) => {
  req.user.player.repeat = req.body.repeat;
  req.user.player.shuffle = req.body.shuffle;
  req.user.path = req.user.restorePath ? req.body.path : "";
  db_manager.updateUserSettings(req.user, () => {
    res.status(202).end();
  });
});

module.exports = router;
