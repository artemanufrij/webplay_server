var express = require("express");
var cors = require("cors");
var cookieParser = require("cookie-parser");
var fs = require("fs");
var si = require("systeminformation");
var bodyParser = require("body-parser");
var passport = require("passport");
var JwtStrategy = require("passport-jwt").Strategy;
var ExtractJwt = require("passport-jwt").ExtractJwt;

let secret = "secret";

var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme("jwt");
opts.secretOrKey = secret;

passport.use(
  new JwtStrategy(opts, (user, done) => {
    db_manager.userByName(user.name, user => {
      if (!user) {
        return done(null, false);
      } else {
        return done(null, user);
      }
    });
  })
);

// CONFIG AND STATUS OBJECTS
var config = JSON.parse(fs.readFileSync("./config.json", "utf8"));
if (!fs.existsSync(config.cache_folder)) {
  fs.mkdirSync(config.cache_folder);
}
config.base_path = __dirname;
var status = {
  scanning_music: false,
  scanning_video: false
};

var lists = {
  audio_quality: ["64", "96", "128", "192", "256", "320"],
  video_quality: ["realtime", "good", "best"],
  user_role: ["admin", "moderator", "user"],
  lang: ["ENG", "GER", "RUS"]
};

si.cpu(function(data) {
  config.cpu_cores = data.cores;
});

exports.config = config;
exports.status = status;
exports.lists = lists;
exports.passport = passport;
exports.secret = secret;

require("./services/cleanup_utiles");
var db_manager = require("./services/db_manager");

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

var originsWhitelist = [
  "http://localhost:8080", //this is my front-end url for development
  "https://media.anufrij.de",
  "https://core.anufrij.de"
];
var corsOptions = {
  origin: function(origin, callback) {
    var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
    callback(null, isWhitelisted);
  },
  credentials: true
};

app.use(cors(corsOptions));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  passport.authenticate("jwt", { session: false });
  next();
});

// ROUTERS
var {
  albumRouter,
  artistRouter,
  trackRouter,
  radioRouter,
  boxRouter,
  videoRouter,
  scanRouter,
  userRouter,
  loginRouter,
  statusRouter,
  systemRouter,
  settingsRouter
} = require("./router");

app.use("/api/album", albumRouter);
app.use("/api/artist", artistRouter);
app.use("/api/track", trackRouter);
app.use("/api/radio", radioRouter);
app.use("/api/box", boxRouter);
app.use("/api/video", videoRouter);
app.use("/api/scan", scanRouter);
app.use("/api/user", userRouter);
app.use("/api/status", statusRouter);
app.use("/api/settings", settingsRouter);
app.use("/user", loginRouter);
app.use("/system", systemRouter);

app.listen(config.port, function() {
  process.stdout.write("Server startet on Port " + config.port + "...\n");
  process.stdout.write("Music Folder: " + config.music_folder + "\n");
  process.stdout.write("Video Folder: " + config.video_folder + "\n");
  process.stdout.write("Cache Folder: " + config.cache_folder + "\n");
});
