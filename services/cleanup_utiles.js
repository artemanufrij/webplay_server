var fs = require("fs");
var notifier = require("./notifier");

var server = require("../server");
var config = server.config;

var album_cover_dir = config.cache_folder + "/album_covers/";
var album_cover_32 = album_cover_dir + "32/";
var album_cover_96 = album_cover_dir + "96/";
var album_cover_64 = album_cover_dir + "64/";
var album_cover_128 = album_cover_dir + "128/";
var album_cover_256 = album_cover_dir + "256/";

var artist_cover_dir = config.cache_folder + "/artist_covers/";
var artist_cover_128 = artist_cover_dir + "128/";
var artist_cover_256 = artist_cover_dir + "256/";
var artist_cover_512 = artist_cover_dir + "512/";

var box_cover_dir = config.cache_folder + "/box_covers/";
var box_cover_32 = box_cover_dir + "32/";
var box_cover_64 = box_cover_dir + "64/";
var box_cover_128 = box_cover_dir + "128/";
var box_cover_256 = box_cover_dir + "256/";

var video_cover_dir = config.cache_folder + "/video_covers/";
var video_cache_dir = config.cache_folder + "/realtime/";

var audio_cache_dir = config.cache_folder + "/audio_cache/";
var audio_cache_128 = audio_cache_dir + "128/";

notifier.on("track_deleted", id => {
  fs.unlink(audio_cache_128 + id + ".mp3", () => {});
});

notifier.on("artist_deleted", id => {
  fs.unlink(artist_cover_128 + id, () => {});
  fs.unlink(artist_cover_256 + id, () => {});
  fs.unlink(artist_cover_512 + id, () => {});
});

notifier.on("album_deleted", id => {
  fs.unlink(album_cover_32 + id, () => {});
  fs.unlink(album_cover_64 + id, () => {});
  fs.unlink(album_cover_96 + id, () => {});
  fs.unlink(album_cover_128 + id, () => {});
  fs.unlink(album_cover_256 + id, () => {});
});

notifier.on("box_deleted", id => {
  fs.unlink(box_cover_32 + id, () => {});
  fs.unlink(box_cover_64 + id, () => {});
  fs.unlink(box_cover_128 + id, () => {});
  fs.unlink(box_cover_256 + id, () => {});
});

notifier.on("video_deleted", id => {
  fs.unlink(video_cover_dir + "/" + id + ".png", () => {});
});
