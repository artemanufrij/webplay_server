var request = require("request");
var notifier = require("./notifier");

var box_cover_queue = [];
var box_cover_requst_is_running = false;

var key = "3f3692d9c336994625a838a95b9a2ed0";
var poster_root = "https://image.tmdb.org/t/p/w500";

let options = {
  headers: {
    "User-Agent":
      "WebPlay/0.1.0 (https://gitlab.com/artemanufrij/webplay_client)"
  }
};

exports.find_box_cover = function(box) {
  box_cover_queue.push(box);
  console.log("TMDB Box: " + box.title);
  run_box_cover_request();
};

async function run_box_cover_request() {
  console.log("started request for box covers");
  if (box_cover_requst_is_running) {
    return;
  }
  box_cover_requst_is_running = true;
  while (box_cover_queue && box_cover_queue.length > 0) {
    await sleep(1500);
    let box = box_cover_queue.shift();
    console.log("SHIFT box: " + box.title);

    let elements = check_for_season(box.title);
    if (elements.season > 0) {
      cover_by_season_number(box, elements);
    } else {
      cover_by_movie_title(box);
    }
  }
  box_cover_requst_is_running = false;
}

function cover_by_movie_title(box) {
  options.url = `https://api.themoviedb.org/3/search/movie?api_key=${key}&query=${
    box.title
  }&page=1&include_adult=false`;
  if (box.year > 0) {
    options.url = `https://api.themoviedb.org/3/search/movie?api_key=${key}&query=${
      box.title
    }&page=1&primary_release_year=${box.year}&include_adult=false`;
  }

  request(options, (err, res, body) => {
    if (err) {
      console.log(err);
      return;
    }
    if (res.statusCode != 200) {
      return;
    }

    let json = JSON.parse(body);
    if (
      json.results &&
      json.results.length > 0 &&
      json.results[0].poster_path
    ) {
      box.tmdb = poster_root + json.results[0].poster_path;
      notifier.emit("found_movie_db_box_cover", box);
    }
  });
}

function cover_by_season_number(box, elements) {
  options.url = `https://api.themoviedb.org/3/search/tv?api_key=${key}&query=${
    elements.title
  }&page=1`;

  request(options, (err, res, body) => {
    if (err) {
      console.log(err);
      return;
    }
    if (res.statusCode != 200) {
      return;
    }

    let json = JSON.parse(body);

    if (json.results && json.results.length > 0) {
      let id = json.results[0].id;
      cover_by_tv_id(id, elements.season, box);
    }
  });
}

function cover_by_tv_id(id, season, box) {
  options.url = `https://api.themoviedb.org/3/tv/${id}/season/${season}?api_key=${key}`;

  request(options, (err, res, body) => {
    if (err) {
      console.log(err);
      return;
    }
    if (res.statusCode != 200) {
      return;
    }

    let json = JSON.parse(body);
    if (json.poster_path) {
      box.tmdb = poster_root + json.poster_path;
      notifier.emit("found_movie_db_box_cover", box);
    }
  });
}

function check_for_season(title) {
  let return_value = { title: title, season: 0 };
  let regex_season = /\s*\-?\s*seasons?\s*\d+$/gi;

  let result = regex_season.exec(title);
  if (result) {
    return_value.title = title.replace(result[0], "");

    regex_season_no = /\d*$/g;
    result = regex_season_no.exec(title);
    return_value.season = parseInt(result[0]);
  }

  return return_value;
}

function sleep(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
}
