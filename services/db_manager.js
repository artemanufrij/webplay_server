var mongo = require("mongodb");
var bcrypt = require("bcryptjs");
var ffmpeg = require("fluent-ffmpeg");
var fs = require("fs");

var notifier = require("./notifier");

var server = require("../server");
var config = server.config;

var url = "mongodb://localhost:27017/";
var database = "webplay";
var dbo;

mongo.connect(url, { useNewUrlParser: true }, (err, db) => {
  if (err) {
    throw err;
  }
  dbo = db.db(database);
  dbo.collection("tracks").createIndex({ path: 1 });
  dbo
    .collection("tracks")
    .createIndex({ album_id: 1, "disk.no": 1, "track.no": 1 });
  dbo.collection("albums").createIndex({ artist_id: 1, year: 1, title: 1 });
  dbo.collection("videos").createIndex({ box_id: 1, title: 1 });
  process.stdout.write("Database connected");
});

notifier.on("metadata_excluded", item => {
  insert_artist_if_not_exists(item.artist, artist => {
    insert_album_if_not_exists(item, artist, album => {
      insert_track_if_not_exists(item, album);
    });
  });
});

notifier.on("pathdata_excluded", item => {
  insert_box_if_not_exists(item, box => {
    insert_video_if_not_exists(item, box);
  });
});

/*
MUSIC
*/
exports.track_exists = function(path, callback) {
  dbo
    .collection("tracks")
    .find({ path: path.replace(config.music_folder, "") })
    .limit(1)
    .toArray((err, result) => {
      callback(result.length > 0);
    });
};

exports.artist_count = function(callback) {
  return dbo.collection("artists").countDocuments(callback);
};

exports.album_count = function(callback) {
  return dbo.collection("albums").countDocuments(callback);
};

exports.track_count = function(callback) {
  return dbo.collection("tracks").countDocuments(callback);
};

exports.box_count = function(callback) {
  return dbo.collection("boxes").countDocuments(callback);
};

exports.video_count = function(callback) {
  return dbo.collection("videos").countDocuments(callback);
};

exports.artists = function(callback) {
  dbo
    .collection("artists")
    .find({})
    .sort({ name: 1 })
    .toArray((err, result) => {
      result.forEach(item => {
        item.cover256 = "";
        item.cover512 = "";
      });
      callback(result);
    });
};

exports.artist = function(id, path_output, callback) {
  let request = [
    { $match: { _id: new mongo.ObjectId(id) } },
    {
      $lookup: {
        from: "albums",
        localField: "_id",
        foreignField: "artist_id",
        as: "albums"
      }
    },
    {
      $unwind: { path: "$albums" }
    },
    {
      $lookup: {
        from: "tracks",
        localField: "albums._id",
        foreignField: "album_id",
        as: "albums.tracks"
      }
    },
    {
      $group: {
        _id: "$_id",
        name: { $first: "$name" },
        albums: { $push: "$albums" }
      }
    }
  ];
  if (!path_output) {
    request.push({
      $project: {
        "albums.tracks.path": path_output
      }
    });
  }

  dbo
    .collection("artists")
    .aggregate(request)
    .toArray((err, result) => {
      let cover = config.cache_folder + "/artist_covers/256/" + id;

      if (fs.existsSync(cover)) {
        let bitmap = fs.readFileSync(cover);
        result[0].cover256 =
          "data:image/png;base64, " + Buffer.from(bitmap).toString("base64");
        result[0].cover512 = "";
      }

      result[0].albums.forEach(album => {
        cover =
          config.cache_folder + "/album_covers/32/" + album._id.toString();
        if (fs.existsSync(cover)) {
          let bitmap = fs.readFileSync(cover);
          album.cover32 =
            "data:image/png;base64, " + Buffer.from(bitmap).toString("base64");
        }
      });
      callback(result[0]);
    });
};

exports.deleteArtist = function(artist, callback) {
  dbo.collection("artists").deleteOne({ _id: artist._id }, err => {
    if (err) throw err;
    callback();
  });
};

exports.emptyArtists = function(callback) {
  dbo
    .collection("artists")
    .aggregate([
      {
        $lookup: {
          from: "albums",
          localField: "_id",
          foreignField: "artist_id",
          as: "albums"
        }
      }
    ])
    .toArray((err, result) => {
      callback(result.filter(f => !f.albums || f.albums.length == 0));
    });
};

exports.albums = function(callback) {
  dbo
    .collection("albums")
    .aggregate([
      {
        $lookup: {
          from: "artists",
          localField: "artist_id",
          foreignField: "_id",
          as: "parent"
        }
      },
      { $unwind: "$parent" },
      { $sort: { "parent.name": 1, year: 1, title: 1 } },
      {
        $project: {
          artist_id: false
        }
      }
    ])
    .toArray((err, result) => {
      result.forEach(item => {
        item.cover32 = "";
        item.cover128 = "";
        item.cover256 = "";
      });
      callback(result);
    });
};

exports.album = function(id, path_output, callback) {
  let request = [
    {
      $lookup: {
        from: "tracks",
        localField: "_id",
        foreignField: "album_id",
        as: "tracks"
      }
    },
    { $match: { _id: new mongo.ObjectId(id) } }
  ];
  if (!path_output) {
    request.push({
      $project: {
        "tracks.path": path_output,
        "tracks.album_id": false,
        artist_id: false
      }
    });
  }

  dbo
    .collection("albums")
    .aggregate(request)
    .toArray((err, result) => {
      if (!result[0].cover32 || !result[0].cover128) {
        let cover = config.cache_folder + "/album_covers/32/" + id;
        if (fs.existsSync(cover)) {
          let bitmap = fs.readFileSync(cover);
          result[0].cover32 =
            "data:image/png;base64, " + Buffer.from(bitmap).toString("base64");
          update_album(result[0]);
        }
        cover = config.cache_folder + "/album_covers/128/" + id;
        if (fs.existsSync(cover)) {
          let bitmap = fs.readFileSync(cover);
          result[0].cover128 =
            "data:image/png;base64, " + Buffer.from(bitmap).toString("base64");
          update_album(result[0]);
        }
      }

      result[0].cover256 = "";
      callback(result[0]);
    });
};

exports.deleteAlbum = function(album, callback) {
  dbo.collection("albums").deleteOne({ _id: album._id }, err => {
    if (err) throw err;
    callback();
  });
};

exports.emptyAlbums = function(callback) {
  dbo
    .collection("albums")
    .aggregate([
      {
        $lookup: {
          from: "tracks",
          localField: "_id",
          foreignField: "album_id",
          as: "tracks"
        }
      }
    ])
    .toArray((err, result) => {
      callback(result.filter(f => !f.tracks || f.tracks.length == 0));
    });
};

exports.tracks = function(callback) {
  dbo
    .collection("tracks")
    .find()
    .toArray((err, result) => {
      callback(result);
    });
};

exports.track = function(id, callback) {
  dbo
    .collection("tracks")
    .findOne({ _id: new mongo.ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.deleteTrack = function(track, callback) {
  dbo.collection("tracks").deleteOne(track, err => {
    if (err) throw err;
    callback();
  });
};

/*
RADIOS
*/
exports.radios = function(callback) {
  dbo
    .collection("radios")
    .find({})
    .sort({ name: 1 })
    .toArray((err, result) => {
      callback(result);
    });
};

exports.addRadio = function(radio, callback) {
  dbo.collection("radios").updateOne(
    { url: radio.url },
    {
      $set: {
        name: radio.name,
        url: radio.url
      }
    },
    { upsert: true },
    err => {
      if (err) throw err;
      dbo.collection("radios").findOne({ url: radio.url }, (err, result) => {
        if (err) throw err;
        callback(result);
      });
    }
  );
};

exports.deleteRadio = function(id, callback) {
  dbo
    .collection("radios")
    .deleteOne({ _id: new mongo.ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

/*
VIDEO
*/
exports.boxes = function(callback) {
  dbo
    .collection("boxes")
    .find({})
    .sort({ title: 1 })
    .toArray((err, result) => {
      result.forEach(item => {
        item.cover128 = "";
        item.cover256 = "";
      });
      callback(result);
    });
};

exports.emptyBoxes = function(callback) {
  dbo
    .collection("boxes")
    .aggregate([
      {
        $lookup: {
          from: "videos",
          localField: "_id",
          foreignField: "box_id",
          as: "videos"
        }
      }
    ])
    .toArray((err, result) => {
      callback(result.filter(f => !f.videos || f.videos.length == 0));
    });
};

exports.box = function(id, path_output, callback) {
  dbo
    .collection("boxes")
    .aggregate([
      {
        $lookup: {
          from: "videos",
          localField: "_id",
          foreignField: "box_id",
          as: "videos"
        }
      },
      { $match: { _id: new mongo.ObjectId(id) } },
      {
        $project: {
          "videos.path": path_output
        }
      }
    ])
    .toArray((err, result) => {
      if (!result[0].cover128) {
        let cover = config.cache_folder + "/box_covers/128/" + id;
        if (fs.existsSync(cover)) {
          let bitmap = fs.readFileSync(cover);
          result[0].cover128 =
            "data:image/png;base64, " + Buffer.from(bitmap).toString("base64");
          update_box(result[0]);
        }
      }
      result[0].cover256 = "";

      result[0].videos.forEach(item => {
        let cover =
          config.cache_folder +
          "/video_covers" +
          "/" +
          item._id.toString() +
          ".png";

        if (fs.existsSync(cover)) {
          let bitmap = fs.readFileSync(cover);
          item.thumbnail =
            "data:image/png;base64, " + Buffer.from(bitmap).toString("base64");
        } else {
          item.thumbnail = "";
        }
      });
      callback(result[0]);
    });
};

exports.deleteBox = function(box, callback) {
  dbo.collection("boxes").deleteOne({ _id: box._id }, err => {
    if (err) throw err;
    callback();
  });
};

exports.videos = function(callback) {
  dbo
    .collection("videos")
    .find()
    .toArray((err, result) => {
      result.forEach(item => {
        item.thumbnail = "";
      });
      callback(result);
    });
};

exports.video_exists = function(path, callback) {
  let short_path = path.replace(config.video_folder, "");
  dbo
    .collection("videos")
    .find({ path: short_path })
    .limit(1)
    .toArray((err, result) => {
      if (err) throw err;
      callback(result.length > 0);
    });
};

exports.video = function(id, callback) {
  dbo
    .collection("videos")
    .findOne({ _id: new mongo.ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.deleteVideo = function(video, callback) {
  dbo.collection("videos").deleteOne({ _id: video._id }, err => {
    if (err) throw err;
    callback();
  });
};

exports.createThumbnail = function(video) {
  create_thumbnail(video);
};

/*
USER
*/

exports.users = function(callback) {
  dbo
    .collection("users")
    .find({}, { password: false })
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.addUser = function(user, callback) {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(user.password, salt, function(err, hash) {
      user.password = hash;
      if (!user.roles) {
        user.roles = ["user"];
      }
      user.player = {};
      dbo.collection("users").insertOne(user, (err, result) => {
        if (err) throw err;
        callback(result);
      });
    });
  });
};

exports.deleteUser = function(id, callback) {
  dbo
    .collection("users")
    .deleteOne({ _id: new mongo.ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.updateUserRole = function(user, callback) {
  dbo
    .collection("users")
    .updateOne({ name: user.name }, { $set: { roles: user.roles } }, err => {
      if (err) throw err;
      callback();
    });
};

exports.updateUserAccess = function(name) {
  dbo
    .collection("users")
    .updateOne(
      { name: name },
      { $set: { last_access: new Date().toLocaleString() } }
    );
};

exports.updateUserPassword = function(user, newPassowrd) {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newPassowrd, salt, function(err, hash) {
      dbo
        .collection("users")
        .updateOne({ name: user.name }, { $set: { password: hash } });
    });
  });
};

exports.updateUserConfig = function(user, body, callback) {
  dbo.collection("users").updateOne(
    { name: user.name },
    {
      $set: {
        restorePath: body.restorePath,
        mobile_bpm: body.mobile_bpm,
        desktop_bpm: body.desktop_bpm,
        video_lang: body.video_lang,
        video_quality: body.video_quality
      }
    },
    err => {
      if (err) throw err;
      callback();
    }
  );
};

exports.updateUserSettings = function(user, callback) {
  dbo
    .collection("users")
    .updateOne(
      { _id: user._id },
      { $set: { player: user.player, path: user.path } },
      err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      }
    );
};

exports.userByName = function(name, callback) {
  dbo.collection("users").findOne({ name: name }, (err, result) => {
    if (err) throw err;
    callback(result);
  });
};

exports.userById = function(id, callback) {
  dbo
    .collection("users")
    .findOne({ _id: new mongo.ObjectId(id) }, (err, result) => {
      if (err) throw err;
      callback(err, result);
    });
};

/*
HISTORY
*/

exports.historyList = function(id, callback) {
  dbo
    .collection("history")
    .find({ userId: id })
    .sort({ _id: -1 })
    .toArray((err, result) => {
      if (err) throw err;
      callback(result);
    });
};

exports.updateHistory = function(id, item, callback) {
  dbo
    .collection("history")
    .deleteOne({ userId: id, containerId: item.id }, err => {
      if (err) throw err;
      dbo
        .collection("history")
        .insertOne(
          { userId: id, containerId: item.id, containerType: item.type },
          err => {
            if (err) throw err;
            callback();
          }
        );
    });
};

/*
FINCTIONS
*/

function insert_artist_if_not_exists(artist, callback) {
  dbo.collection("artists").findOne({ name: artist }, (err, result) => {
    if (err) throw err;
    if (result) {
      callback(result);
    } else {
      dbo
        .collection("artists")
        .updateOne(
          { name: artist },
          { $set: { name: artist } },
          { upsert: true },
          err => {
            if (err) throw err;
            insert_artist_if_not_exists(artist, callback);
          }
        );
    }
  });
}

function insert_album_if_not_exists(item, artist, callback) {
  dbo
    .collection("albums")
    .findOne({ artist_id: artist._id, title: item.album }, (err, result) => {
      if (err) throw err;
      if (result) {
        callback(result);
      } else {
        dbo.collection("albums").updateOne(
          { artist_id: artist._id, title: item.album },
          {
            $set: {
              artist_id: artist._id,
              title: item.album,
              year: item.year
            }
          },
          { upsert: true },
          err => {
            if (err) throw err;
            insert_album_if_not_exists(item, artist, callback);
          }
        );
      }
    });
}

function update_album(album, callback) {
  dbo
    .collection("albums")
    .updateOne(
      { _id: album._id },
      { $set: { cover32: album.cover32, cover128: album.cover128 } },
      { upsert: false },
      err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      }
    );
}

function insert_track_if_not_exists(item, album, callback) {
  let short_path = item.path.replace(config.music_folder, "");
  dbo.collection("tracks").updateOne(
    { path: short_path },
    {
      $set: {
        title: item.title,
        path: short_path,
        album_id: album._id,
        duration: item.duration,
        bitrate: item.bitrate,
        mime: item.mime,
        disk: item.disk,
        track: item.track,
        genre: item.genre
      }
    },
    { upsert: true },
    err => {
      if (err) throw err;
      dbo.collection("tracks").findOne({ path: short_path }, (err, result) => {
        if (err) throw err;
        if (callback) {
          callback(result);
        }
      });
    }
  );
}

function insert_box_if_not_exists(box, callback) {
  let short_path = box.box_path.replace(config.music_folder, "");
  dbo
    .collection("boxes")
    .findOne({ title: box.box, year: box.year }, (err, result) => {
      if (err) throw err;
      if (result) {
        callback(result);
      } else {
        dbo
          .collection("boxes")
          .updateOne(
            { title: box.box },
            { $set: { title: box.box, path: short_path, year: box.year } },
            { upsert: true },
            err => {
              if (err) throw err;
              insert_box_if_not_exists(box, callback);
            }
          );
      }
    });
}

function update_box(box, callback) {
  dbo
    .collection("boxes")
    .updateOne(
      { _id: box._id },
      { $set: { cover128: box.cover128 } },
      { upsert: false },
      err => {
        if (err) throw err;
        if (callback) {
          callback();
        }
      }
    );
}

function insert_video_if_not_exists(item, box, callback) {
  item.path = item.path.replace(config.video_folder, "");
  dbo.collection("videos").updateOne(
    { path: item.path },
    {
      $set: {
        title: item.title,
        path: item.path,
        box_id: box._id,
        mime: item.mime
      }
    },
    { upsert: true },
    err => {
      if (err) throw err;
      dbo.collection("videos").findOne({ path: item.path }, (err, result) => {
        if (err) throw err;
        if (callback) {
          callback(result);
        }
        create_thumbnail(result);
      });
    }
  );
}

function create_thumbnail(video) {
  let cover =
    config.cache_folder + "/video_covers" + "/" + video._id.toString() + ".png";
  if (fs.existsSync(cover)) {
    process.stdout.write("Thumbnail exists '" + video.title + "'\n");
  } else {
    ffmpeg(config.video_folder + video.path)
      .on("end", () => {
        process.stdout.write("Thumbnail for '" + video.title + "' created\n");
      })
      .on("error", err => {
        process.stdout.write("Thumbnail error: " + err.message);
      })
      .screenshot(
        {
          timestamps: ["20%"],
          filename: video._id.toString(),
          folder: config.cache_folder + "/video_covers",
          size: "346x180"
        },
        () => {}
      );
  }
}
