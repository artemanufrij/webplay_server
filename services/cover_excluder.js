var fs = require("fs");
var path = require("path");
var sharp = require("sharp");
var request = require("request");
var notifier = require("./notifier");
var db_manager = require("./db_manager");
var music_brainz = require("./music_brainz");
var the_movie_db = require("./the_movie_db");

var server = require("../server");
var status = server.status;
var config = server.config;
var album_cover_dir = config.cache_folder + "/album_covers/";
var album_cover_32 = album_cover_dir + "32/";
var album_cover_64 = album_cover_dir + "64/";
var album_cover_96 = album_cover_dir + "96/";
var album_cover_128 = album_cover_dir + "128/";
var album_cover_256 = album_cover_dir + "256/";
var album_cover_512 = album_cover_dir + "512/";
var artist_cover_dir = config.cache_folder + "/artist_covers/";
var artist_cover_128 = artist_cover_dir + "128/";
var artist_cover_256 = artist_cover_dir + "256/";
var artist_cover_512 = artist_cover_dir + "512/";
var box_cover_dir = config.cache_folder + "/box_covers/";
var box_cover_32 = box_cover_dir + "32/";
var box_cover_64 = box_cover_dir + "64/";
var box_cover_128 = box_cover_dir + "128/";
var box_cover_256 = box_cover_dir + "256/";
var video_cover_dir = config.cache_folder + "/video_covers/";

var jpg_q = 90;

/*
ALBUM CACHE
*/
if (!fs.existsSync(album_cover_dir)) {
  fs.mkdirSync(album_cover_dir);
}
if (!fs.existsSync(album_cover_32)) {
  fs.mkdirSync(album_cover_32);
}
if (!fs.existsSync(album_cover_64)) {
  fs.mkdirSync(album_cover_64);
}
if (!fs.existsSync(album_cover_96)) {
  fs.mkdirSync(album_cover_96);
}
if (!fs.existsSync(album_cover_128)) {
  fs.mkdirSync(album_cover_128);
}
if (!fs.existsSync(album_cover_256)) {
  fs.mkdirSync(album_cover_256);
}
if (!fs.existsSync(album_cover_512)) {
  fs.mkdirSync(album_cover_512);
}
/*
ARTIST CACHE
*/
if (!fs.existsSync(artist_cover_dir)) {
  fs.mkdirSync(artist_cover_dir);
}
if (!fs.existsSync(artist_cover_128)) {
  fs.mkdirSync(artist_cover_128);
}
if (!fs.existsSync(artist_cover_256)) {
  fs.mkdirSync(artist_cover_256);
}
if (!fs.existsSync(artist_cover_512)) {
  fs.mkdirSync(artist_cover_512);
}
/*
BOX CACHE
*/
if (!fs.existsSync(box_cover_dir)) {
  fs.mkdirSync(box_cover_dir);
}
if (!fs.existsSync(box_cover_32)) {
  fs.mkdirSync(box_cover_32);
}
if (!fs.existsSync(box_cover_64)) {
  fs.mkdirSync(box_cover_64);
}
if (!fs.existsSync(box_cover_128)) {
  fs.mkdirSync(box_cover_128);
}
if (!fs.existsSync(box_cover_256)) {
  fs.mkdirSync(box_cover_256);
}
/*
VIDEO CACHE
*/
if (!fs.existsSync(video_cover_dir)) {
  fs.mkdirSync(video_cover_dir);
}

notifier.on("exclude_metadata_finished", () => {
  process.stdout.write("music scann finished…");
  checkMissingAlbumCovers();
  checkMissingArtistCovers();
  status.scanning_music = false;
});

notifier.on("exclude_pathdata_finished", () => {
  process.stdout.write("video scann finished…");
  checkMissingBoxCovers();
  status.scanning_video = false;
});

notifier.on("metadata_picture_excluded", album => {
  let tmp_file = album_cover_dir + album._id;
  fs.writeFileSync(tmp_file, album.picture[0].data);

  save_album_cover(tmp_file, album._id.toString(), has_errors => {
    if (has_errors) {
      process.stdout.write("HAS ERRORS: " + album.title);
      music_brainz.find_album_cover(album);
    } else {
      fs.unlinkSync(tmp_file);
    }
  });
});

notifier.on("no_metadata_picture_excluded", album => {
  music_brainz.find_album_cover(album);
});

notifier.on("found_music_brainz_album_cover", result => {
  process.stdout.write("MB album result: " + result.mb);

  let album_id = result.album._id.toString();
  let tmp_file = album_cover_dir + album_id;

  let writer = fs.createWriteStream(tmp_file);

  writer.on("finish", () => {
    save_album_cover(tmp_file, album_id, has_errors => {
      if (!has_errors) {
        fs.unlinkSync(tmp_file);
      }
    });
  });

  request(result.mb).pipe(writer);
});

notifier.on("found_music_brainz_artist_cover", result => {
  process.stdout.write("MB artist result: " + result.mb);

  let artist_id = result.artist._id.toString();
  let tmp_file = artist_cover_dir + artist_id;
  let writer = fs.createWriteStream(tmp_file);

  writer.on("finish", () => {
    save_artist_cover(tmp_file, artist_id, has_errors => {
      if (!has_errors) {
        try {
          fs.unlinkSync(tmp_file);
        } catch (err) {
          process.stdout.write(err);
        }
      }
    });
  });

  request(result.mb).pipe(writer);
});

notifier.on("found_movie_db_box_cover", box => {
  process.stdout.write("TMVD box result: " + box.tmdb);

  let box_id = box._id.toString();
  let tmp_file = box_cover_dir + box_id;

  let writer = fs.createWriteStream(tmp_file);

  writer.on("finish", () => {
    save_box_cover(tmp_file, box_id, has_errors => {
      if (!has_errors) {
        fs.unlinkSync(tmp_file);
      }
    });
  });

  request(box.tmdb).pipe(writer);
});

function checkMissingAlbumCovers() {
  process.stdout.write("started cover excluding for albums…");
  db_manager.albums(albums => {
    albums.forEach(album => {
      let album_id = album._id.toString();
      let cover = album_cover_dir + "512/" + album_id;

      if (fs.existsSync(cover)) {
        return;
      }

      db_manager.album(album_id, true, album_tracks => {
        if (!album_tracks || !album_tracks.tracks) {
          return;
        }
        album.tracks = album_tracks.tracks;
        let cover_found = false;
        album.tracks.forEach(track => {
          if (cover_found) {
            return;
          }

          let file_path = config.music_folder + track.path;
          let directory = path.dirname(file_path);

          config.album_cover_files.forEach(file => {
            let img = directory + "/" + file;
            if (fs.existsSync(img)) {
              cover_found = true;
              save_album_cover(img, album_id);
              return;
            }
          });
        });

        if (!cover_found) {
          export_from_id3(album);
        }
      });
    });
  });
}

function export_from_id3(album) {
  album.tracks.forEach(track => {
    track.parent = album;
  });
  notifier.emit("album_cover_request", album.tracks[0]);
}

function save_album_cover(file, album_id, callback = null) {
  let cover_32 = album_cover_32 + album_id;
  let cover_64 = album_cover_64 + album_id;
  let cover_96 = album_cover_96 + album_id;
  let cover_128 = album_cover_128 + album_id;
  let cover_256 = album_cover_256 + album_id;
  let cover_512 = album_cover_512 + album_id;

  sharp(file)
    .resize({ width: 512, height: 512, fit: "cover" })
    .toFile(cover_512)
    .then(() => {
      sharp(cover_512)
        .resize(256, 256)
        .toFile(cover_256)
        .then(() => {
          sharp(cover_256)
            .resize(128, 128)
            .toFile(cover_128)
            .then(() => {
              sharp(cover_128)
                .resize(96, 96)
                .toFile(cover_96)
                .then(() => {
                  sharp(cover_96)
                    .resize(64, 64)
                    .toFile(cover_64)
                    .then(() => {
                      sharp(cover_64)
                        .resize(32, 32)
                        .toFile(cover_32, () => {
                          if (callback) {
                            callback(false);
                          }
                        });
                    });
                });
            });
        });
    })
    .catch(err => {
      process.stdout.write(err);
      process.stdout.write("ERROR save_album_cover: " + file);
      if (callback) {
        callback(true);
      }
    });
}

exports.checkMissingArtistCovers = function() {
  checkMissingArtistCovers();
};

function checkMissingArtistCovers() {
  process.stdout.write("started cover excluding for artists…");
  db_manager.artists(artists => {
    artists.forEach(artist => {
      let artist_id = artist._id.toString();
      let cover = artist_cover_dir + "128/" + artist_id;

      if (fs.existsSync(cover)) {
        return;
      }

      db_manager.artist(artist_id, true, artist_full => {
        if (!artist_full.albums) {
          return;
        }

        let cover_found = false;
        artist_full.albums.forEach(album => {
          album.tracks.forEach(track => {
            if (cover_found) {
              return;
            }

            config.artist_cover_files.forEach(file => {
              let file_path = config.music_folder + track.path;
              let directory = path.dirname(file_path);
              let img = directory + "/" + file;
              if (fs.existsSync(img)) {
                cover_found = true;
                save_artist_cover(img, artist_id);
                return;
              }

              directory = path.dirname(directory);
              img = directory + "/" + file;
              if (fs.existsSync(img)) {
                cover_found = true;
                save_artist_cover(img, artist_id);
                return;
              }

              directory = path.dirname(directory);
              img = directory + "/" + file;
              if (fs.existsSync(img)) {
                cover_found = true;
                save_artist_cover(img, artist_id);
                return;
              }
            });
          });
        });

        if (!cover_found) {
          music_brainz.find_artist_cover(artist_full);
        }
      });
    });
  });
}

function save_artist_cover(file, artist_id, callback = null) {
  let cover_512 = artist_cover_512 + artist_id;
  let cover_256 = artist_cover_256 + artist_id;
  let cover_128 = artist_cover_128 + artist_id;

  sharp(file)
    .resize({ width: 1024, height: 512, fit: "cover" })
    .toFile(cover_512)
    .then(() => {
      sharp(cover_512)
        .resize(512, 256)
        .toFile(cover_256)
        .then(() => {
          sharp(cover_256)
            .resize(256, 128)
            .toFile(cover_128)
            .then(() => {
              if (callback) {
                callback(false);
              }
            });
        });
    })
    .catch(() => {
      process.stdout.write("ERROR save_artist_cover: " + file);
      if (callback) {
        callback(true);
      }
    });
}

function checkMissingBoxCovers() {
  process.stdout.write("started cover excluding for boxes…");
  db_manager.boxes(boxes => {
    boxes.forEach(box => {
      let cover_found = false;
      let box_id = box._id.toString();
      let cover = box_cover_dir + "128/" + box_id;

      if (fs.existsSync(cover)) {
        return;
      }

      let file_path = config.video_folder + box.path;
      let directory = file_path;

      config.box_cover_files.forEach(file => {
        let img = directory + "/" + file;
        if (fs.existsSync(img)) {
          cover_found = true;
          save_box_cover(img, box_id);
          return;
        }
      });

      if (!cover_found) {
        the_movie_db.find_box_cover(box);
      }
    });
  });
}

function save_box_cover(file, box_id, callback = null) {
  process.stdout.write("save box cover: " + file);
  let cover_256 = box_cover_256 + box_id;
  let cover_128 = box_cover_128 + box_id;
  let cover_64 = box_cover_64 + box_id;
  let cover_32 = box_cover_32 + box_id;

  sharp(file)
    .jpeg({ quality: jpg_q })
    .resize({ width: 256, height: 362, fit: "cover" })
    .toFile(cover_256)
    .then(() => {
      sharp(cover_256)
        .jpeg({ quality: jpg_q })
        .resize({ width: 128, height: 181, fit: "cover" })
        .toFile(cover_128)
        .then(() => {
          sharp(cover_128)
            .resize(64, 90)
            .toFile(cover_64)
            .then(() => {
              sharp(cover_64)
                .resize(32, 45)
                .toFile(cover_32)
                .then(() => {
                  if (callback) {
                    callback(false);
                  }
                });
            });
        });
    })
    .catch(() => {
      process.stdout.write("ERROR save_box_cover: " + file);
      if (callback) {
        callback(true);
      }
    });
}
